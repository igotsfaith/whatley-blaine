# Text and Media

*“Every Connection has a price; the one thing you can be sure of is that, sooner or later, 
you will have to pay.”*

There is subliminal messages in every form of meida
The presense of **bolded** or _italised_ words indicate they are more important or want more attention than
other words in the piece. We must be aware of the effect different types of text can have on us and 
our perception of different messages. 
![caption](https://contentinsights.com/wp-content/uploads/2019/01/How-yellow-journalism-evolved-and-why-you-should-care-about-its-influence-1.png)

In this picture the differetn font and sizes of the text and headlines draws your attention to certain
words indicitating specific levels of importance despite what the words actually say. Throuhg using 
markdown I can control both the ***size*** and _emphasis_ on different texts in order to direct your 
attention to specific words or messages. The markdown application allows me to easily connect to you
through these words and subliminal messages I'm placing in this piece of text. Markdown is a nice 
medium between Word and straight up HTML coding by giving the freedom and liberty
to think about decisions and specific messages within the words.

Shaviro talks primarily about how media is so utterly integerated in our lives and everything we 
decide to do._“No matter what position you seek to occupy, that position will be located somewhere on 
the network’s grid.”_ The decision to go off the grid requires the presence of a grid in the first place. 
One must acknowledge the extent technology is intertwined in everyone’s lives to grasp its invasive
presence. There are various levels of how we allow technology to determine our lives, but we can only
control our reaction to technology. You can’t improve or change the system, but for your own sanity 
you must try to ignore this invasive presence that stalks every decision of our lives. Technology is 
allowing me to transmit my message to you right now, through this screen. But the meaning of the text
isn't nearly as important 

According to Shaviro in Connected, _“The meaning of the message cannot be isolated from its mode of 
propagation, from the way it harasses me, attacks me, or parasitically invades me.”_ The way a message 
reaches a person is much more important than what the information actually holds. Today we can see 
this in advertisements and how commercials hold subliminal messages. Even simple songs can have hidden 
messages behind the steady beat or nice rhythm that most people might not consciously 
think about. Most modern day pop songs have enduendos and references to drugs or sexual intimicies that
most children might not realize and some adults might not pick up due to the medium they are portrayed 
in. Even the popular show SpongebobSquarepants has become infamous to having multiple dirty referances 
and jokes littered throughout each episode that most children don't pick up on these explicit jokes, and 
due to the innocent nature of cartoons many adults are none the wiser to these sexual or dark themes in
the show. The message within this simple kids show must be viewed in context and cannot be isolated
from the show and subliminal messages the show portrays in the first place.